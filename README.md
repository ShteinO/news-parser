Используемые библиотеки:
    Microsoft.Data.SqlClient
    Swashbuckle.AspNetCore - исключительно для удобства проверяющего
    Microsoft.Extensions.DependencyInjection.Abstractions
    Microsoft.Extensions.Configuration.Abstractions

Приложение написано так, что если у вас запущенн MSSQL Server Express и там сохранилась родная БД Master, то код сам развернет базу, схему, таблицу и полностью их заполнил.

Парсится сайт https://kapital.kz/economic?page= , парсер проходит по трем страницам и собирает 30 новостей с названием, датой и также переходит на страницу конкретной новости что бы спарсить текст новости.

appsettings.json
        {
    "ConnectionStrings": {
        "DefaultConnection": "Server=DESKTOP-UOA3JIL\\SQLEXPRESS;Database=master;Trusted_Connection=True;MultiSubnetFailover=True;Trust Server Certificate=true;",
        "NewsConnection": "Server=DESKTOP-UOA3JIL\\SQLEXPRESS;Database=news_parser;Trusted_Connection=True;MultiSubnetFailover=True;Trust Server Certificate=true;"
    }
    Состоит из двух строк подключения, прошу заменить название Server=НазваниеСервера. По остальным парам, пришлось эксперментировать потому что у ноутбука было несколько конфликтов с доступом к БД и сертификатом, которые не решились кроме как допольнительными парами ключ значение.
