﻿using Microsoft.AspNetCore.Mvc;
using NewsParser.Application.Contracts.Services;
using NewsParser.Domain.Entities.Dto;

namespace NewsParser.API.Controllers;

[ApiController]
[Route("[controller]")]
public class NewsController : Controller
{
    private readonly INewsParserService _parser;
    private const string Url = "https://kapital.kz/economic?page=";

    public NewsController(INewsParserService parser)
    {
        _parser = parser;
    }

    [HttpGet("GetAllNews")]
    public List<NewsDto> GetAllNews()
    {
        return  _parser.GetAllNews(Url);
    } 

    [HttpGet("GetNewsByDate")]
    public List<NewsDto> GetByDates(DateTime from, DateTime to)
    {
        try
        {
            DateTime dateFrom;
            DateTime dateTo;
            if (DateTime.TryParse(from.ToString(), out dateFrom) && DateTime.TryParse(to.ToString(), out dateTo))
            {
                var newsDto = _parser.GetByDate(dateFrom, dateTo);
                if (newsDto == null || !newsDto.Any())
                    return GetAllNews();
                else
                    return newsDto;
            }
            return new List<NewsDto>();
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            throw;
        }
    }
    
    [HttpGet("GetNewsByQuery")]
    public List<NewsDto> GetBySearchQuery(string request)
    {
        try
        {
            return _parser.GetBySearchQuery(request);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            throw;
        }
    }
    
    [HttpGet("GetTopTenWords")]
    public List<TopTenDto> GetTopTenWords()
    {
        try
        {
            return _parser.GetTopTen();
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            throw;
        }
    }
}