﻿namespace NewsParser.Domain.Entities.Dto;

public class TopTenDto
{
    public string Value { get; set; }
    public int Frequency { get; set; }
}