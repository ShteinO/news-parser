﻿using System;

namespace NewsParser.Domain.Entities.Dto;

public class NewsDto
{
    public string Title { get; set; }
    public string NewsContent { get; set; }
    public DateTime PostDate { get; set; }
}