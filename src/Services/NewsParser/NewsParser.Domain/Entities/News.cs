﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NewsParser.Domain.Entities;

public class News
{
    [Key]
    public int Id { get; set; }
    public string Title { get; set; }
    public string NewsContent { get; set; }
    public DateTime PostDate { get; set; }
}