﻿using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NewsParser.Domain.Entities.Dto;

namespace NewsParser.Infrastructure.Data;

public class DbConfig
{
    private readonly IConfiguration _configuration;
    
    public DbConfig(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public bool CheckDataBase(string dbName)
    {
        try
        {
            var connectionString = _configuration.GetConnectionString("DefaultConnection");

            var commandText = $"select count(*) from master.dbo.sysdatabases where name=@{dbName}";

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                using (var sqlCommand = new SqlCommand(commandText, sqlConnection))
                {
                    sqlCommand.Parameters.Add($"{dbName}", System.Data.SqlDbType.NVarChar).Value = dbName;
                    sqlConnection.Open();
                    return Convert.ToInt32( sqlCommand.ExecuteScalar()) == 1;
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return false;
        }
    }
    
    public void DbCreate()
    {
        try
        {
            using( SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.CommandText = "CREATE DATABASE news_parser";
                command.Connection = connection;
                command.ExecuteNonQuery();
                command.CommandText = "CREATE SCHEMA news_schema";
                command.Connection = connection;
                connection.ChangeDatabase("news_parser");
                command.ExecuteNonQuery();
                command.CommandText = "CREATE TABLE news_parser.news_schema.news (Id INT PRIMARY KEY IDENTITY, Title NVARCHAR(100) NOT NULL, NewsContent NVARCHAR(1000) NOT NULL, PostDate DATETIME2 NOT NULL)";
                command.Connection = connection;
                command.ExecuteNonQuery();
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            throw;
        }
    }

    public List<NewsDto> GetAllDto()
    {
        return new List<NewsDto>();
    }

}