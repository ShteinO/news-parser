﻿using System;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NewsParser.Application.Contracts.Repository;
using NewsParser.Application.Contracts.Services;
using NewsParser.Infrastructure.Data;
using NewsParser.Infrastructure.Persistence;

namespace NewsParser.Infrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructureServices(this IServiceCollection services)
    {
        try
        {
            services.AddTransient<INewsRepository, NewsRepository>();
            services.AddTransient<INewsParserService, NewsParserService>();
            return services;
        }
        catch (SqlException e)
        {
            Console.WriteLine(e.Message);
            throw;
        }
    }
}