﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using NewsParser.Application.Contracts.Repository;
using NewsParser.Domain.Entities;
using NewsParser.Domain.Entities.Dto;
using NewsParser.Infrastructure.Data;

namespace NewsParser.Infrastructure.Persistence;

public class NewsRepository : INewsRepository
{
    private readonly IConfiguration _configuration;

    public NewsRepository(IConfiguration configuration)
    {
        _configuration = configuration;
    }
    
    public List<NewsDto> GetAllNews()
    {
        try
        {
            DbConfig dbСonfig = new DbConfig(_configuration);
            if (!dbСonfig.CheckDataBase("news_parser"))
            {
                dbСonfig.DbCreate();
                return  new List<NewsDto>();
            }
            else
            {
                var connectionString = _configuration.GetConnectionString("NewsConnection");
                List<NewsDto> newsDto = new List<NewsDto>();
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string sql = "SELECT * FROM news_schema.news";
                    SqlCommand command = new SqlCommand(sql, connection);
                    SqlDataReader reader = command.ExecuteReader();
 
                    while(reader.Read())
                    {
                        string title = reader.GetString(1);
                        string newsContent = reader.GetString(2);
                        DateTime postDate = reader.GetDateTime(3);

                        NewsDto newDto = new NewsDto()
                        {
                            Title = title,
                            NewsContent = newsContent,
                            PostDate = postDate
                        };
                        newsDto.Add(newDto);
                    }
                }

                return newsDto;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            throw;
        }
        
    }

    public List<NewsDto> GetNewsByDates(DateTime from, DateTime to)
    {
        try
        {
            DbConfig dbСonfig = new DbConfig(_configuration);
            if (!dbСonfig.CheckDataBase("news_parser"))
            {
                dbСonfig.DbCreate();
                return  new List<NewsDto>();
            }
            else
            {
                var connectionString = _configuration.GetConnectionString("NewsConnection");
                List<NewsDto> newsDto = new List<NewsDto>();
                string fromDate = from.ToString("O");
                string toDate = to.ToString("O");
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string sql = $"SELECT * FROM news_schema.news WHERE PostDate BETWEEN '{fromDate}' AND '{toDate}'";
                    SqlCommand command = new SqlCommand(sql, connection);
                    SqlDataReader reader = command.ExecuteReader();
 
                    while(reader.Read())
                    {
                        string title = reader.GetString(1);
                        string newsContent = reader.GetString(2);
                        DateTime postDate = reader.GetDateTime(3);

                        NewsDto newDto = new NewsDto()
                        {
                            Title = title,
                            NewsContent = newsContent,
                            PostDate = postDate
                        };
                        newsDto.Add(newDto);
                    }
                }

                return newsDto;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            throw;
        }
    }
    
    public List<TopTenDto> GetTopTenWords()
    {
        try
        {
            DbConfig dbСonfig = new DbConfig(_configuration);
            if (!dbСonfig.CheckDataBase("news_parser"))
            {
                dbСonfig.DbCreate();
                return new List<TopTenDto>();
            }
            else
            {
                var connectionString = _configuration.GetConnectionString("NewsConnection");
                List<TopTenDto> topTenDto = new List<TopTenDto>();
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string sql = "WITH Words AS( SELECT id, value FROM news_schema.news CROSS APPLY STRING_SPLIT (NewsContent, ' ')) SELECT TOP 10 value,COUNT(*) as frequency FROM Words Where LEN(value) > 3 GROUP BY value ORDER BY frequency DESC";
                    SqlCommand command = new SqlCommand(sql, connection);
                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        string value = reader.GetString(0);
                        int frequency = reader.GetInt32(1);

                        TopTenDto topTenWords = new TopTenDto()
                        {
                            Value = value,
                            Frequency = frequency
                        };
                        topTenDto.Add(topTenWords);
                    }

                    return topTenDto;
                }
            }
        }
        catch(Exception e)
        {
            Console.WriteLine(e.Message);
            throw;
        }
    }

    public List<NewsDto> GetBySearchQuery(string searchQuery)
    {
        try
        {
            DbConfig dbСonfig = new DbConfig(_configuration);
            if (!dbСonfig.CheckDataBase("news_parser"))
            {
                dbСonfig.DbCreate();
                return new List<NewsDto>();
            }
            else
            {
                var connectionString = _configuration.GetConnectionString("NewsConnection");
                List<NewsDto> newsDto = new List<NewsDto>();
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string sql = $"SELECT * FROM news_schema.news WHERE NewsContent LIKE '%{searchQuery}%'";
                    SqlCommand command = new SqlCommand(sql, connection);
                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        string title = reader.GetString(1);
                        string newsContent = reader.GetString(2);
                        DateTime postDate = reader.GetDateTime(3);

                        NewsDto newDto = new NewsDto()
                        {
                            Title = title,
                            NewsContent = newsContent,
                            PostDate = postDate
                        };
                        newsDto.Add(newDto);
                    }

                    return newsDto;
                }
            }
        }
        catch(Exception e)
        {
            Console.WriteLine(e.Message);
            throw;
        }
    }

    public void AddNews(List<News> news)
    {
        try
        {
            var connectionString = _configuration.GetConnectionString("NewsConnection");

            var commandText = @"INSERT INTO news_schema.news VALUES (@Title, @NewsContent, @PostDate)";;

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                using (var sqlCommand = new SqlCommand(commandText, sqlConnection))
                {
                    sqlConnection.Open();
                    sqlCommand.Parameters.Add("@Title", SqlDbType.NVarChar, 100);
                    sqlCommand.Parameters.Add("@NewsContent", SqlDbType.NVarChar, 1000);
                    sqlCommand.Parameters.Add("@PostDate", SqlDbType.DateTime);
                    for (int i = 0; i < news.Count; i++)
                    {
                        sqlCommand.Parameters["@Title"].Value = news[i].Title;
                        sqlCommand.Parameters["@NewsContent"].Value = news[i].NewsContent;
                        sqlCommand.Parameters["@PostDate"].Value = news[i].PostDate;
                        sqlCommand.ExecuteNonQuery();
                    }
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            throw;
        }
    }
}