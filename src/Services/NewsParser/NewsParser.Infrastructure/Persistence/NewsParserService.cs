﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using NewsParser.Application.Contracts.Repository;
using NewsParser.Application.Contracts.Services;
using NewsParser.Domain.Entities;
using NewsParser.Domain.Entities.Dto;

namespace NewsParser.Infrastructure.Persistence;

public class NewsParserService : INewsParserService
{
    public string TitleRegexTemplate { get; set; } = "(?:<a.*?class=\"main-news__name\".*?>)(.*?)(?:<\\/a>)";
    public string DateRegexTemplate { get; set; } = "(?:<time.*?class=\"information-article__date\"\\sdatetime=\"..........\\s........\\s.....\".*?>)(.*?)(<\\/time>)";
    public string ContentRegexTemplate { get; set; } = @"(?i)<div[^>]*?>\s*<p>([^<]*)<";
    public string SiteUrl { get; set; } = "https://kapital.kz/";
    public int Page { get; set; } = 1;

    private readonly INewsRepository _repository;
    
    public NewsParserService(INewsRepository repository)
    {
        _repository = repository;
    }
    public List<NewsDto> GetAllNews(string url)
    {
        if (_repository.GetAllNews() == null || !_repository.GetAllNews().Any())
        {
            for (int i = 1; i < 4; i++)
            {
                _repository.AddNews(ParseField(url + i.ToString()));
            }
        }

        return _repository.GetAllNews();
    }

    public List<NewsDto> GetByDate(DateTime from, DateTime to)
    {
        return _repository.GetNewsByDates(from, to);
    }

    public List<NewsDto> GetBySearchQuery(string request)
    {
        return _repository.GetBySearchQuery(request);
    }

    public List<TopTenDto> GetTopTen()
    {
        return _repository.GetTopTenWords();
    }

    public List<News> ParseField(string url)
    {
        try
        {
            using (HttpClientHandler handler = new HttpClientHandler())
            {
                using (HttpClient htmlToString = new HttpClient(handler))
                {
                    var response = htmlToString.GetAsync(url).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var html = response.Content.ReadAsStringAsync().Result;
                        
                        var titles = Regex.Matches(html, TitleRegexTemplate);
                        var postDates = Regex.Matches(html, DateRegexTemplate);
                        var titlesUrl = string.Join(',', titles);
                        
                        var listTitles = titles.Cast<Match>().SelectMany(o => o.Groups.Cast<Capture>()).Select(c => c.Value).Where(c => !c.Contains("main-news__name")).ToList();
                        var listDates = postDates.Cast<Match>().SelectMany(o => o.Groups[1].Captures).Select(c => c.Value).Where(c=>!c.Contains("-")).ToList();
                        var listLinksOnNews = GetLinks(titlesUrl);
                        List<string> listNewsText = new List<string>();
                        for (int i = 0; i < listLinksOnNews.Count; i++)
                        {
                            listNewsText.Add(ParseNewsText(string.Concat(SiteUrl, listLinksOnNews[i])));
                        }

                        return CollectEntity(listTitles, listNewsText, listDates);
                    }

                    return null;
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            throw;
        }
    }

    public string ParseNewsText(string url)
    {
        try
        {
            using (HttpClientHandler handler = new HttpClientHandler())
            {
                using (HttpClient htmlToString = new HttpClient(handler))
                {
                    var responce = htmlToString.GetAsync(url).Result;
                    if (responce.IsSuccessStatusCode)
                    {
                        var html = responce.Content.ReadAsStringAsync().Result;
                        var titles = Regex.Matches(html, ContentRegexTemplate);
                        var listTitles = titles.Cast<Match>().SelectMany(o => o.Groups.Cast<Capture>()).Where(c => !c.Value.Contains("<div")).Select(c => c.Value).ToList();
                        var newsText = string.Join(" ", listTitles);
                        return newsText;
                    }
                }
            }

            return null;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            throw;
        }
    }

    public List<string> GetLinks(string titlesBeforeCut)
    {
        try
        {
            List<string> titlesLink = new List<string>();
            string titleLinkTemplate = @"href\s*=\s*(?:'([^']*)'|""([^""]*)""|([^\s>]+))";
            var urlOnNews = Regex.Matches(titlesBeforeCut, titleLinkTemplate);
            titlesLink = urlOnNews.Cast<Match>().SelectMany(o => o.Groups.Cast<Capture>()).Select(c => c.Value).Where(c => c.Contains("economic") && !c.Contains("href")).ToList();
            return titlesLink;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            throw;
        }
    }

    public List<News> CollectEntity(List<string> title, List<string> newsContent, List<string> date)
    {
        try
        {
            for (int i = 0; i < date.Count; i++)
            {
                date[i] = date[i].Remove(date[i].IndexOf('&'), date[i].IndexOf(';') - date[i].IndexOf('&')).Replace("; ", "");
            }
            List<News> newsEntities = new List<News>();
            DateTime dateTime;
            for(int i = 0; i < title.Count; i++)
            {
                newsEntities.Add(new News()
                {
                    Title = title[i],
                    NewsContent = newsContent[i],
                    PostDate = DateTime.Parse(date[i])
                });
            }

            return newsEntities;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            throw;
        }
    }
}