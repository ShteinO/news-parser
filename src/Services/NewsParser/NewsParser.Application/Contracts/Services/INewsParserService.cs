﻿using System;
using System.Collections.Generic;
using NewsParser.Domain.Entities;
using NewsParser.Domain.Entities.Dto;

namespace NewsParser.Application.Contracts.Services;

public interface INewsParserService
{
    string TitleRegexTemplate { get; set; }
    string DateRegexTemplate { get; set; }
    string ContentRegexTemplate { get; set; }
    string SiteUrl { get; set; }
    int Page { get; set; }

    List<NewsDto> GetAllNews(string url);
    List<NewsDto> GetByDate(DateTime from, DateTime to);
    List<NewsDto> GetBySearchQuery(string request);
    List<TopTenDto> GetTopTen();
    List<News> ParseField(string url);
    string ParseNewsText(string url);
    List<string> GetLinks(string titlesBeforeCut);
    List<News> CollectEntity(List<string> title, List<string> newsContent, List<string> date);
}