﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NewsParser.Domain.Entities;
using NewsParser.Domain.Entities.Dto;

namespace NewsParser.Application.Contracts.Repository;

public interface INewsRepository
{
    List<NewsDto> GetAllNews();
    List<NewsDto> GetNewsByDates(DateTime from, DateTime to);
    List<NewsDto> GetBySearchQuery(string searchQuery);
    List<TopTenDto> GetTopTenWords();
    void AddNews(List<News> newsDto);
}