--�������� �� news_parser
CREATE DATABASE news_parser;

--�������� ����� news_schema
CREATE SCHEMA news_schema

--�������� ������� news_schema.news
CREATE TABLE news_parser.news_schema.news (Id INT PRIMARY KEY IDENTITY, Title NVARCHAR(100) NOT NULL, NewsContent NVARCHAR(1000) NOT NULL, PostDate DATETIME2 NOT NULL);

--������ �� ��� ������  � ������� news
SELECT *
FROM news_schema.news;

--������ �� ������ ����� ��������� ����������
SELECT *
FROM news_schema.news
WHERE PostDate BETWEEN '2022-12-26 09:54:00' AND '2022-12-28 10:54:00';

--������ �� ������ �� ���������� ������� ������ ������ �������
SELECT Title, NewsContent
FROM news_schema.news
WHERE NewsContent LIKE '%�������������%'
Group BY Title;

--������ �� ��� 10 ���� ����� ������������� ���� ������ �� 3 ��������
WITH Words AS(
    SELECT id, value
    FROM news_schema.news CROSS APPLY STRING_SPLIT(NewsContent, ' ')
)
SELECT  TOP 10 value,
    COUNT(*) as frequency
FROM 
    Words
Where LEN(value) > 3 --����� �������� ����� ������ ��������� ����� ���������������� ��������
GROUP BY value
ORDER BY frequency DESC